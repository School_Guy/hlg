package de.schoolguy.main;

/**
 * Default Description
 *
 * @author SchoolGuy <matrixfueller@gmail.com>
 * @version 1.0
 */
interface DATENELEMENT {
    public void Ausgeben();

    public boolean IstKleinerAls(DATENELEMENT dvergleich);

    public boolean SchluesselIstGleich(String s);
}
