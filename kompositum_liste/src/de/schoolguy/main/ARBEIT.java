package de.schoolguy.main;

/**
 * Default Description
 *
 * @author Schoolguy <matrixfueller@gmail.com>
 * @version 1.0
 */

class ARBEIT {
    // Attribute

    // Konstruktor
    public ARBEIT() {
        LISTE liste = new LISTE();
        KUNDE k1 = new KUNDE("Huber", "Eduard");
        KUNDE k2 = new KUNDE("Meier", "Paul");
        KUNDE k3 = new KUNDE("Müller", "Inge");
        KUNDE k4 = new KUNDE("Daxenberger", "Franz");
        KUNDE k5 = new KUNDE("Goethe", "Johann");
        KUNDE k6 = new KUNDE("Mirsberger", "Hans");

        // 1. Durchgang:
        System.out.println("Vorne einfügen: ");
        liste.VorneEinfuegen(k1);
        liste.VorneEinfuegen(k2);
        liste.VorneEinfuegen(k3);
        liste.VorneEinfuegen(k4);
        liste.VorneEinfuegen(k5);
        //liste.VorneEinfuegen(k6);
        liste.InformationAusgeben();

        // Suchen
        System.out.println("Suchen: ");
        liste.Suchen("Göthe");   // Rückgabewert wird nicht benötigt
        liste.Suchen("Goethe");
        System.out.println();

        // Knoten entfernen
        System.out.println("Knoten k4 entfernen:");
        liste.KnotenEntfernen(k4);
        liste.InformationAusgeben();
        System.out.println();

        System.out.println("Knoten k6 entfernen:");
        liste.KnotenEntfernen(k6);
        liste.InformationAusgeben();


    }

    // Methoden

}

