package de.schoolguy.main;

/**
 * Default Description
 *
 * @author Schoolguy <matrixfueller@gmail.com>
 * @version 1.0
 */
class KNOTEN extends LISTENELEMENT {
    // Attribute
    private DATENELEMENT element;
    private LISTENELEMENT nachfolger;

    // Konstruktor
    KNOTEN(DATENELEMENT d, LISTENELEMENT l) {
        element = d;
        nachfolger = l;
    }

    // Methoden
    // rekursiver Durchlauf durch die Liste und Einfügen eines Knotens mit Datenelement dneu an der letzten Stelle
    public KNOTEN HintenEinfuegen(DATENELEMENT dneu) {
        nachfolger = nachfolger.HintenEinfuegen(dneu);
        return this;
    }

    // rekursiver Durchlauf durch die Liste und Einfügen eines Knotens mit Datenelement dneu vor einem Datenelement d_vergleich
    public KNOTEN EinfuegenVor(DATENELEMENT dneu, DATENELEMENT d_vergleich) {
        if (element == d_vergleich) {
            KNOTEN kneu;
            kneu = new KNOTEN(dneu, this);
            return kneu;
        } else {
            nachfolger = nachfolger.EinfuegenVor(dneu, d_vergleich);
            return this;
        }
    }

    // rekursiver Durchlauf durch die Liste und sortiertes Einfügen eines Knotens mit Datenelement dneu an der richtigen Stelle
    public KNOTEN SortiertEinfuegen(DATENELEMENT dneu) {
        if (element.IstKleinerAls(dneu)) {
            nachfolger = nachfolger.SortiertEinfuegen(dneu);
            return this;
        } else {
            KNOTEN kneu;
            kneu = new KNOTEN(dneu, this);
            return kneu;
        }
    }

    // rekursiver Durchlauf durch die Liste und Entfernen des Knotens mit dem entsprechenden Inhalt
    public LISTENELEMENT KnotenEntfernen(DATENELEMENT dvergleich) {
        if (element == dvergleich) {
            return nachfolger;
        } else {
            nachfolger = nachfolger.KnotenEntfernen(dvergleich);
            return this;
        }
    }


    // rekursiver Durchlauf ans Ende der Liste und Entfernen des letzten Knotens
    public LISTENELEMENT EndeEntfernen() {
        // Das Schlüsselwort instanceof überprüft ob eine Objekt von der Klasse ABSCHLUSS:
        // TRUE: der Nachfolger ist der Abschluss, FALSE: das Ende ist noch nicht erreicht
        if (nachfolger instanceof ABSCHLUSS) {
            return nachfolger; //Der Knoten hängst sich selbst aus, indem er seinem Vorgänger seinen Nachfolger übergibt.
        } else {
            nachfolger = nachfolger.EndeEntfernen();
            return this;
        }
    }

    // rekursiver Durchlauf ans Ende der Liste und Rückgabe des letzten Knotens
    public DATENELEMENT EndeGeben(DATENELEMENT d) {
        return nachfolger.EndeGeben(element);
    }

    // rekursiver Durchlauf durch die Liste mit Ausgabe der Inhalte
    public void InformationAusgeben() {
        element.Ausgeben();
        nachfolger.InformationAusgeben();
    }

    // rekursiver Durchlauf der Liste und Suche nach dem vergleichswert,
    public DATENELEMENT Suchen(String vergleichswert) {
        if (element.SchluesselIstGleich(vergleichswert)) {
            System.out.print("Datensatz gefunden!");
            element.Ausgeben();
            return element;
        } else {
            return nachfolger.Suchen(vergleichswert);
        }
    }

    // rekursive Bestimmung der Nachfolgeranzahl
    public int RestlaengeGeben() {
        return nachfolger.RestlaengeGeben() + 1;
    }

    // Gettermethoden
    public DATENELEMENT DatenelementGeben() {
        return element;
    }

    public LISTENELEMENT NachfolgerGeben() {
        return nachfolger;
    }
}

