package de.schoolguy.main;

/**
 * Default Description
 *
 * @author SchoolGuy <matrixfueller@gmail.com>
 * @version 1.0
 */
class LISTE {
    // Attribute
    private LISTENELEMENT anfang;

    // Konstruktor
    LISTE() {
        anfang = new ABSCHLUSS();
    }

    // Methoden
    // Einfügen am Anfang der Liste
    void VorneEinfuegen(DATENELEMENT dneu) {
        KNOTEN kneu;
        kneu = new KNOTEN(dneu, anfang);
        anfang = kneu;
    }

    // Einfügen am Ende der Liste
    public void HintenEinfuegen(DATENELEMENT dneu) {
        anfang = anfang.HintenEinfuegen(dneu);
    }

    // Einfügen vor einem bestimmten Datenelement
    public void EinfuegenVor(DATENELEMENT dneu, DATENELEMENT d_vergleich) {
        anfang = anfang.EinfuegenVor(dneu, d_vergleich);
    }

    // Einfügen, z.B. alphabetisch
    public void SortiertEinfuegen(DATENELEMENT dneu) {
        anfang = anfang.SortiertEinfuegen(dneu);
    }

    // der Knoten mit dem übergebene Inhalt wird gesucht und entfernt
    public void KnotenEntfernen(DATENELEMENT dvergleich) {
        anfang = anfang.KnotenEntfernen(dvergleich);
    }

    // Das erste Listenelement wird ausgegeben und dann gelöscht
    public DATENELEMENT AnfangEntfernen() {
        DATENELEMENT d;
        d = anfang.DatenelementGeben();
        anfang = anfang.NachfolgerGeben();
        return d;
    }

    // Entfernt den letzten Knoten und gibt dessen Inhalt aus
    public DATENELEMENT EndeEntfernen() {
        DATENELEMENT d = anfang.EndeGeben(null);
        anfang = anfang.EndeEntfernen();
        return d;
    }

    // Ein Datenelement mit dem String vergleichswert wird gesucht
    public DATENELEMENT Suchen(String vergleichswert) {
        return anfang.Suchen(vergleichswert);
    }

    // Ausgabe der ganzen Liste
    public void InformationAusgeben() {
        System.out.println("Die Liste enthält " + LaengeGeben() + " Elemente:");
        anfang.InformationAusgeben();
        System.out.println("---- Ende der Liste ----");
        System.out.println();
    }

    // Methode liefert das Datenelement des letzten Knotens der Liste
    public DATENELEMENT EndeGeben() {
        return anfang.EndeGeben(null);
    }

    // Ermittlung der Länge der Liste (rekursiv, ersetzt das Attribut anzahl)
    public int LaengeGeben() {
        return anfang.RestlaengeGeben();
    }
}

