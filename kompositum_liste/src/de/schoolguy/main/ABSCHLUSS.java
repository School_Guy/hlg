package de.schoolguy.main;

/**
 * Default Description
 *
 * @author SchoolGuy <matrixfueller@gmail.com>
 * @version 1.0
 */
class ABSCHLUSS extends LISTENELEMENT {
    // Attribute

    // Konstruktor

    // Methoden
    public KNOTEN HintenEinfuegen(DATENELEMENT dneu) {
        KNOTEN kneu;
        kneu = new KNOTEN(dneu, this);
        return kneu;
    }

    public KNOTEN EinfuegenVor(DATENELEMENT dneu, DATENELEMENT d_vergleich) {
        return HintenEinfuegen(dneu);
    }

    public KNOTEN SortiertEinfuegen(DATENELEMENT dneu) {
        return HintenEinfuegen(dneu);
    }

    public LISTENELEMENT KnotenEntfernen(DATENELEMENT dvergleich) {
        System.out.println("Knoten nicht gefunden, kann nicht gelöscht werden!");
        return this;
    }

    public DATENELEMENT EndeGeben(DATENELEMENT d) {
        return d;
    }

    // folgende Methode muss implementiert sein, wird aber nie benötigt
    public LISTENELEMENT EndeEntfernen() {
        return this;
    }

    public void InformationAusgeben() {
    }

    public DATENELEMENT Suchen(String vergleichswert) {
        System.out.println("Datensatz " + vergleichswert + " nicht gefunden!");
        return null;
    }

    public int RestlaengeGeben() {
        return 0;
    }

    public DATENELEMENT DatenelementGeben() {
        return null;
    }

    public LISTENELEMENT NachfolgerGeben() {
        return this;
    }
}

