package de.schoolguy.main;

/**
 * Default Description
 *
 * @author SchoolGuy <matrixfueller@gmail.com>
 * @version 1.0
 */
class KARTE implements DATENELEMENT {
    // Attribute
    private String rang;
    private int anzahl;
    private double einzelpreis;

    // Konstruktor
    KARTE(String neuerRang, int neueAnzahl, double neuerPreis) {
        rang = neuerRang;
        anzahl = neueAnzahl;
        einzelpreis = neuerPreis;
    }

    // Methoden
    // Getter- und Setter-Methoden
    public void rangSetzen(String neuerRang) {
        rang = neuerRang;
    }

    public String rangGeben() {
        return rang;
    }

    public void anzahlSetzen(int neueAnzahl) {
        anzahl = neueAnzahl;
    }

    public int anzahlGeben() {
        return anzahl;
    }

    public void einzelpreisSetzen(double neuerPreis) {
        einzelpreis = neuerPreis;
    }

    public double einzelpreisGeben() {
        return einzelpreis;
    }

    public int Vergleichen(DATENELEMENT vergleichsObjekt) {
        KARTE vergleichsKarte = (KARTE) vergleichsObjekt;
        return rang.compareTo(vergleichsKarte.rang);
    }

    public boolean IstKleinerAls(DATENELEMENT dvergleich) {
        if (Vergleichen(dvergleich) < 0)
            return true;         // Element ist kleiner
        else
            return false;     // Element ist größer
    }

    public boolean IstGroesserAls(DATENELEMENT dvergleich) {
        if (Vergleichen(dvergleich) < 0)
            return false;         // Element ist kleiner
        else
            return true;     // Element ist größer
    }

    // Vergleichsschlüssel ist der Rang
    public boolean SchluesselIstGleich(String s) {
        if (rang == s)
            return true;         // Vergleichselement ist gleich dem Namen
        else
            return false;
    }

    public double GesamtpreisBerechnen() {
        return anzahl * einzelpreis;
    }

    public void Ausgeben() {
        System.out.println("Rang der Karte: " + rang + "," + anzahl + " Stück für je " + einzelpreis + ", Gesamtpreis:  " + GesamtpreisBerechnen());
    }
}

