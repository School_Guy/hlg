package de.schoolguy.main;

/**
 * Default Description
 *
 * @author SchoolGuy <matrixfueller@gmail.com>
 * @version 1.0
 */
abstract class LISTENELEMENT {
    abstract KNOTEN HintenEinfuegen(DATENELEMENT dneu);

    abstract KNOTEN EinfuegenVor(DATENELEMENT dneu, DATENELEMENT d_vergleich);

    abstract KNOTEN SortiertEinfuegen(DATENELEMENT dneu);

    abstract LISTENELEMENT KnotenEntfernen(DATENELEMENT dvergleich);

    abstract DATENELEMENT EndeGeben(DATENELEMENT d);

    abstract LISTENELEMENT EndeEntfernen();  // Trick: instanceof

    abstract void InformationAusgeben();

    abstract DATENELEMENT Suchen(String vergleichswert);

    abstract int RestlaengeGeben();

    abstract DATENELEMENT DatenelementGeben();

    abstract LISTENELEMENT NachfolgerGeben();
}
