package de.schoolguy.main;

/**
 * Default Description
 *
 * @author SchoolGuy <matrixfueller@gmail.com>
 * @version 1.0
 */
class KUNDE implements DATENELEMENT {
    // Attribute
    private String name;
    private String vorname;

    // Konstruktor
    public KUNDE(String neuerName, String neuerVorname) {
        name = neuerName;
        vorname = neuerVorname;
    }

    // Methoden
    public boolean IstKleinerAls(DATENELEMENT dvergleich) {
        if (Vergleichen(dvergleich) < 0)
            return true;         // Element ist kleiner
        else
            return false;     // Element ist größer
    }

    // Vergleichsschlüssel ist der Name
    public boolean SchluesselIstGleich(String s) {
        if (name == s)
            return true;         // Vergleichselement ist gleich dem Namen
        else
            return false;
    }

    public int Vergleichen(DATENELEMENT vergleichsObjekt) {
        // Es wird ein negativer Wert zurückgegeben, falls der aktuelle Wert kleiner als der Vergleichwert ist
        KUNDE vergleichsKunde = (KUNDE) vergleichsObjekt;
        int ergebnis;

        ergebnis = name.compareTo(vergleichsKunde.name);
        if (ergebnis == 0)
            ergebnis = vorname.compareTo(vergleichsKunde.vorname);

        return ergebnis;
    }

    public void Ausgeben() {
        System.out.println("Kunde: " + name + ", " + vorname);
    }

    // Getter- und Setter-Methoden
    public void NameSetzen(String neuerName) {
        name = neuerName;
    }

    public void VornameSetzen(String neuerVorname) {
        vorname = neuerVorname;
    }

    public String NameGeben() {
        return name;
    }

    public String VornameGeben() {
        return vorname;
    }
}

