package de.schoolguy.main;

/**
 * Default Description
 *
 * @author SchoolGuy <matrixfueller@gmail.com>
 * @version 1.0
 */

public class Knoten extends Baumelement {
    private Baumelement nachfolgerLinks, nachfolgerRechts;
    private Kunde k;

    public Knoten(Baumelement links, Baumelement rechts, Kunde kNeu) {
        super();
        k = kNeu;
        nachfolgerLinks = links;
        nachfolgerRechts = rechts;
    }

    public int gibAnzahlAbHier() {
        return nachfolgerLinks.gibAnzahlAbHier() + nachfolgerRechts.gibAnzahlAbHier() + 1;
    }

    public Kunde gibDaten() {
        return k;
    }

    public Baumelement sortiereEin(Kunde kNeu) {
        if (k.istVor(kNeu)) {
            // wenn der hier verwaltete Kunde alphabetisch vor kNeu steht
            // wird die Methode des rechten Nachfolgers aufgerufen
            nachfolgerRechts = nachfolgerRechts.sortiereEin(kNeu);
        } else {
            // andernfalls wird die Methode des linken Nachfolgers aufgerufen.
            nachfolgerLinks = nachfolgerLinks.sortiereEin(kNeu);
        }
        // Der aktuelle Knoten muss zurückgegeben werden, damit der Vorgänger
        // ihn als seinen neuen Nachfolger registrieren kann
        return this;
    }

    public void gibAusAbHierInorder() {
        nachfolgerLinks.gibAusAbHierInorder();
        k.gibAus();
        nachfolgerRechts.gibAusAbHierInorder();

    }

    public void gibAusAbHierPreorder() {
        k.gibAus();
        nachfolgerLinks.gibAusAbHierPreorder();
        nachfolgerRechts.gibAusAbHierPreorder();
    }

    public void gibAusAbHierPostorder() {
        nachfolgerLinks.gibAusAbHierPostorder();
        nachfolgerRechts.gibAusAbHierPostorder();
        k.gibAus();

    }

    public Baumelement entferne(Kunde kAlt) {
        if (kAlt == k) {
            if (nachfolgerLinks.gibAnzahlAbHier() == 0) {
                return nachfolgerRechts;
            } else if (nachfolgerRechts.gibAnzahlAbHier() == 0) {
                return nachfolgerLinks;
            } else {
                k = nachfolgerRechts.sucheMin(null);
                nachfolgerRechts = nachfolgerRechts.entferne(k);
            }
        } else if (k.istVor(kAlt)) {
            nachfolgerRechts = nachfolgerRechts.entferne(kAlt);
        } else {
            nachfolgerLinks = nachfolgerLinks.entferne(kAlt);
        }
        return this;
    }

    public Kunde sucheAbHier(String name) {
        int temp = name.compareTo(k.gibName());
        if (temp == 0) {
            return k;
        } else if (temp < 0) {
            return nachfolgerLinks.sucheAbHier(name);
        } else {
            return nachfolgerRechts.sucheAbHier(name);
        }

    }

    public Kunde sucheMin(Kunde kVerg) {
        return nachfolgerLinks.sucheMin(k);
    }

}

