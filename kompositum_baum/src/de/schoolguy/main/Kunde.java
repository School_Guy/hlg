package de.schoolguy.main;

/**
 * Default Description
 *
 * @author SchoolGuy <matrixfueller@gmail.com>
 * @version 1.0
 */

public class Kunde {
    private String nachname, vorname;

    public Kunde(String name0, String vorname0) {
        vorname = vorname0;
        nachname = name0;
    }

    public String gibName() {
        return nachname + ", " + vorname;
    }


    public boolean istVor(Kunde k_verg) {
        return (gibName().compareTo(k_verg.gibName()) < 0);
    }

    public boolean istGleich(Kunde k_verg) {
        return (gibName().compareTo(k_verg.gibName()) == 0);
    }

    public void gibAus() {
        System.out.println(vorname + " " + nachname);
    }
}
