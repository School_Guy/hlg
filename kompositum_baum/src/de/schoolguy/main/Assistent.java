package de.schoolguy.main;

/**
 * Default Description
 *
 * @author SchoolGuy <matrixfueller@gmail.com>
 * @version 1.0
 */

public class Assistent {
    private Binaerbaum baum;

    public Assistent() {
        baum = new Binaerbaum();
    }

    public void fuegeHinzu(String nachname, String vorname) {
        Kunde k = new Kunde(nachname, vorname);
        baum.fuegeSortiertEin(k);
        System.out.println("Eingefügt " + k.gibName() + ", jetzt sind es " + baum.gibAnzahl());
    }

    public void erzeugeTestBaum() {
        fuegeHinzu("Huber", "Josef");
        fuegeHinzu("Meier", "Miriam");
        fuegeHinzu("Schmidt", "Susanne");
        fuegeHinzu("Mayer", "Miroslav");
        fuegeHinzu("Adam", "Apfel");
    }

    public void gibAusInorder() {
        baum.gibAlleAusInorder();
    }

    public void gibAusPreorder() {
        baum.gibAlleAusPreorder();
    }

    public void gibAusPostorder() {
        baum.gibAlleAusPostorder();
    }

    public void entferne(String nachname, String vorname) {
        Kunde k = new Kunde(nachname, vorname);
        k = baum.suche(k.gibName());
        baum.loesche(k);
    }

    public void sucheKunde(String name) {
        Kunde temp = baum.suche(name);
        if (temp == null) {
            System.out.println("Gibts nicht!");
        } else {
            System.out.println(temp.gibName() + " gefunden");
        }
    }

}
