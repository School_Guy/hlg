package de.schoolguy.main;

/**
 * Default Description
 *
 * @author SchoolGuy <matrixfueller@gmail.com>
 * @version 1.0
 */

public class Abschluss extends Baumelement {
    public Abschluss() {
        super();
    }

    public int gibAnzahlAbHier() {
        return 0;
    }

    public Kunde gibDaten() {
        return null;
    }

    public Baumelement sortiereEin(Kunde kNeu) {
        return new Knoten(this, new Abschluss(), kNeu);
        // new Abschluss(), damit zwei verschiedene Abschluss-Objekte vorhanden sind.
    }

    public void gibAusAbHierInorder() {
    }

    public void gibAusAbHierPreorder() {
    }

    public void gibAusAbHierPostorder() {
    }

    public Baumelement entferne(Kunde kAlt) {
        return this;
    }

    public Kunde sucheAbHier(String name) {
        return null;
    }

    public Kunde sucheMin(Kunde kVerg) {
        return kVerg;
    }
}

