package de.schoolguy.main;

/**
 * Default Description
 *
 * @author SchoolGuy <matrixfueller@gmail.com>
 * @version 1.0
 */

public abstract class Baumelement {
    public Baumelement() {
    }

    public abstract int gibAnzahlAbHier();

    public abstract Kunde gibDaten();

    public abstract Baumelement sortiereEin(Kunde kNeu);

    public abstract void gibAusAbHierInorder();

    public abstract void gibAusAbHierPreorder();

    public abstract void gibAusAbHierPostorder();

    public abstract Baumelement entferne(Kunde kAlt);

    public abstract Kunde sucheAbHier(String name);

    public abstract Kunde sucheMin(Kunde kVerg);
}
