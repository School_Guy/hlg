package de.schoolguy.main;

/**
 * Default Description
 *
 * @author SchoolGuy <matrixfueller@gmail.com>
 * @version 1.0
 */

public class Binaerbaum {
    private Baumelement wurzel;

    public Binaerbaum() {
        wurzel = new Abschluss();
    }

    public void fuegeSortiertEin(Kunde k) {
        wurzel = wurzel.sortiereEin(k);
    }

    public boolean istLeer() {
        return wurzel instanceof Abschluss;
    }

    public int gibAnzahl() {
        return wurzel.gibAnzahlAbHier();
    }

    public void gibAlleAusInorder() {
        wurzel.gibAusAbHierInorder();
    }

    public void gibAlleAusPreorder() {
        wurzel.gibAusAbHierPreorder();
    }

    public void gibAlleAusPostorder() {
        wurzel.gibAusAbHierPreorder();
    }

    public void loesche(Kunde k) {
        wurzel = wurzel.entferne(k);

    }


    public Kunde suche(String name) {
        return wurzel.sucheAbHier(name);
    }
}
