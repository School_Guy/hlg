package de.schoolguy;

/**
 * Write a description of class HEIMTIER here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class HEIMTIER extends TIER
{
    // Variables
    public boolean isForFood;

    public HEIMTIER(String name,double mass,int age,boolean isForFood)
    {
        setType("Hund");
        setName(name);
        setMass(mass);
        setAge(age);
        this.isForFood = isForFood;
        setNarkoseKosten(30 + (mass * 0.9));
    }

    public void getData ()
    {
        System.out.println ("Das Tier ist ein/e: " + getType ());
        System.out.println ("Der Name des Tieres ist: " + getName());
        System.out.println ("Das Gewicht des Tieres ist: " + getMass());
        System.out.println ("Das Alter des Tieres ist: " + getAge());
        System.out.println ("Das Tier ist als Lebensmittel gedacht: " + isForFood);
        System.out.println ("Eine Narkose fuer das Tier wuerde " + getNarkoseKosten() + " Euro kosten");
    }
}
