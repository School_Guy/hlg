package de.schoolguy;

/**
 * Abstract class TIER - write a description of the class here
 *
 * @author (your name here)
 * @version (version number or date here)
 */
public abstract class TIER
{
    // Variables
    private String type;
    private String name;
    private double mass;
    private int age;
    private double narkoseKosten;

    public abstract void getData ();

    public void setNarkoseKosten (double narkoseKosten)
    {
        this.narkoseKosten = narkoseKosten;
    }

    public double getNarkoseKosten ()
    {
        return narkoseKosten;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getType ()
    {
        return type;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getName ()
    {
        return name;
    }

    public void setMass (double mass)
    {
        this.mass = mass;
    }

    public double getMass ()
    {
        return mass;
    }

    public void setAge (int age)
    {
        this.age = age;
    }

    public int getAge ()
    {
        return age;
    }

}