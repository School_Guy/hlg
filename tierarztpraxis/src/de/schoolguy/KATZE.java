package de.schoolguy;

/**
 * Write a description of class KATZE here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class KATZE extends TIER
{
    // Variables
    public boolean catOnlyInFlat;

    public KATZE(String name,double mass,int age,boolean catOnlyInFlat)
    {
        setType("Hund");
        setName(name);
        setMass(mass);
        setAge(age);
        this.catOnlyInFlat = catOnlyInFlat;
        setNarkoseKosten(30 + (mass * 0.9));
    }

    public void getData ()
    {
        System.out.println ("Das Tier ist ein/e: " + getType());
        System.out.println ("Der Name des Tieres ist: " + getName());
        System.out.println ("Das Gewicht des Tieres ist: " + getMass());
        System.out.println ("Das Alter des Tieres ist: " + getAge());
        System.out.println ("Das Tier wird in der Wohnung gehalten: " + catOnlyInFlat);
        System.out.println ("Eine Narkose fuer das Tier wuerde " + getNarkoseKosten() + " Euro kosten");
    }
}
