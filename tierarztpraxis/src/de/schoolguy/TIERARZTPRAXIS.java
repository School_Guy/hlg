package de.schoolguy;

import java.io.*;
/**
 * Write a description of class TIERAZTPRAXIS here.
 *
 * @author (Enno Gotthold, Maria Vogel, Melinda Roos)
 * @version (V1.0)
 */
public class TIERARZTPRAXIS
{
    //Variables
    TIER [] wartezimmer;
    BufferedReader br;
    short anzahlTiere;

    public TIERARZTPRAXIS() throws IOException
    {
        wartezimmer = new TIER [9999];
        br = new BufferedReader(new InputStreamReader(System.in));
        begruessung();
    }

    public void begruessung () throws IOException
    {
        System.out.println("Willkommen in unserer Tierarztpraxis!");
        System.out.println("Moechten sie ihr Tier abholen oder moechten sie uns eins in die Behandlung geben? (Y/N)");
        String input;
        input = br.readLine();
        if (input.equals("Y")|| input.equals("y"))
        {
            System.out.println("Bitte geben sie die Tierart die sie abgeben moechten an! (Katze, Hund, Heimtier)");
            String inputType = br.readLine();
            addAnimal(inputType);
        } else if (input.equals("N")|| input.equals("n"))
        {
            showWaitingRoom();
        }
    }

    public void addAnimal(String type) throws IOException
    {
        if (type.equals("Hund"))
        {
            System.out.println("Bitte geben sie einen Namen ein: ");
            String name = br.readLine();
            System.out.println("Bitte geben sie einen Alter ein: ");
            String ageString = br.readLine();
            int age;
            age = Integer.parseInt(ageString);
            System.out.println("Bitte geben sie einen Gewicht ein: ");
            String massString = br.readLine();
            double mass = Double.parseDouble(massString);
            System.out.println("Bitte geben sie an ob der Hund reinrassig ist: (true/false)");
            String raceString = br.readLine();
            Boolean race = Boolean.valueOf(raceString);
            HUND Hund = new HUND(name,mass,age,race);
            wartezimmer [anzahlTiere] = Hund;
            anzahlTiere ++;
        } else if (type.equals("Katze")) {
            System.out.println("Bitte geben sie einen Namen ein: ");
            String name = br.readLine();
            System.out.println("Bitte geben sie einen Alter ein: ");
            String ageString = br.readLine();
            int age;
            age = Integer.parseInt(ageString);
            System.out.println("Bitte geben sie einen Gewicht ein: ");
            String massString = br.readLine();
            double mass = Double.parseDouble(massString);
            System.out.println("Bitte geben sie an ob die Katze nur in der Wohnung gehalten wird: (true/false)");
            String raceString = br.readLine();
            Boolean race = Boolean.valueOf(raceString);
            KATZE Katze = new KATZE(name,mass,age,race);
            wartezimmer [anzahlTiere] = Katze;
            anzahlTiere ++;
        } else if (type.equals("Heimtier")) {
            System.out.println("Bitte geben sie einen Namen ein: ");
            String name = br.readLine();
            System.out.println("Bitte geben sie einen Alter ein: ");
            String ageString = br.readLine();
            int age;
            age = Integer.parseInt(ageString);
            System.out.println("Bitte geben sie einen Gewicht ein: ");
            String massString = br.readLine();
            double mass = Double.parseDouble(massString);
            System.out.println("Bitte geben sie an ob das Tier zu einem Lebensmittel verarbeitet wird: (true/false)");
            String raceString = br.readLine();
            Boolean race = Boolean.valueOf(raceString);
            HEIMTIER Heimtier = new HEIMTIER(name,mass,age,race);
            wartezimmer [anzahlTiere] = Heimtier;
            anzahlTiere ++;
        } else {
            System.out.println("Gibt es nicht! Bitte geben sie den Tiertyp nochmal ein.");
            String inputType = br.readLine();
            addAnimal(inputType);
        }
        sprechzimmer();
    }

    public void sprechzimmer ()  throws IOException
    {
        System.out.println("Moegt ihr ein weiteres Tier abgeben? (Y/N)");
        String input;
        input = br.readLine();
        if (input.equals("Y")|| input.equals("y"))
        {
            System.out.println("Bitte geben sie die Tierart die sie abgeben moechten an! (Katze, Hund, Heimtier)");
            String inputType = br.readLine();
            addAnimal(inputType);
        } else if (input.equals("N")|| input.equals("n"))
        {
            showWaitingRoom();
        }
    }

    public void showWaitingRoom () throws IOException
    {
        for (int i = 0;i < anzahlTiere; i++) {
            wartezimmer [i].getData ();
        }
        System.out.println("Das Programm ist nun fertig! Konsole wird geloescht!");
        String input = br.readLine();
        System.out.print('\u000c');
    }
}