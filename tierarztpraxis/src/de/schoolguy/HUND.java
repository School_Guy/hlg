package de.schoolguy;

/**
 * Write a description of class HUND here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class HUND extends TIER
{
    // Variables
    public boolean race;

    public HUND(String name,double mass,int age,Boolean race)
    {
        setType("Hund");
        setName(name);
        setMass(mass);
        setAge(age);
        this.race = race;
        setNarkoseKosten(30 + (mass * 0.9));
    }

    public void getData ()
    {
        System.out.println ("Das Tier ist ein/e: " + getType());
        System.out.println ("Der Name des Tieres ist: " + getName());
        System.out.println ("Das Gewicht des Tieres ist: " + getMass());
        System.out.println ("Das Alter des Tieres ist: " + getAge());
        System.out.println ("Das Tier ist von der Rasse: " + race);
        System.out.println ("Eine Narkose fuer das Tier wuerde " + getNarkoseKosten() + " Euro kosten");
    }
}