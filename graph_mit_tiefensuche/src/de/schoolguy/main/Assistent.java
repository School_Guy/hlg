package de.schoolguy.main;

/**
 * Default Description
 *
 * @author SchoolGuy <matrixfueller@gmail.com>
 * @version 1.0
 */

public class Assistent {
    private Graph autobahnnetz;

    public Assistent() {
        autobahnnetz = new Graph(20);
    }

    public void erzeugeKnoten(String name) {
        autobahnnetz.erzeugeKnoten(name);
    }

    public void verbinde(String name1, String name2, int bewertung) {
        if (!autobahnnetz.verbinde(name1, name2, bewertung)) {
            System.out.println("Fehler beim Erzeugen der Verbindung!");
        }

    }

    public void erzeugeNetz() {
        erzeugeKnoten("Altd");
        erzeugeKnoten("BA");
        erzeugeKnoten("BT");
        erzeugeKnoten("ER");
        erzeugeKnoten("N");
        erzeugeKnoten("N/Ost");
        erzeugeKnoten("N/Süd");
        erzeugeKnoten("OPfW");
        erzeugeKnoten("R");
        erzeugeKnoten("SW");
        erzeugeKnoten("WÜ");
        verbinde("Altd", "N", 8);
        verbinde("Altd", "N/Ost", 7);
        verbinde("Altd", "OPfW", 74);
        verbinde("Altd", "R", 80);
        verbinde("BA", "BT", 53);
        verbinde("BA", "ER", 47);
        verbinde("BA", "SW", 70);
        verbinde("BT", "N", 77);
        verbinde("ER", "N", 23);
        verbinde("ER", "N/Süd", 25);
        verbinde("ER", "WÜ", 81);
        verbinde("N", "N/Ost", 8);
        verbinde("N/Ost", "N/Süd", 5);
        verbinde("OPfW", "R", 79);
        verbinde("SW", "WÜ", 30);
    }

    public void schreibeVerbindung(String name) {
        autobahnnetz.schreibeKanten(name);
    }

    public void schreibeMatrix() {
        autobahnnetz.schreibeMatrix();
    }

    public void schreibeAlleKnoten() {
        autobahnnetz.schreibeAlleKnoten();
    }

    public void loescheKnoten(String ort) {
        autobahnnetz.loescheKnoten(ort);
    }

    public void tiefensuche() {
        autobahnnetz.tiefensuche(0);
    }

}

