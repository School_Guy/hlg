package de.schoolguy.main;

/**
 * Default Description
 *
 * @author SchoolGuy <matrixfueller@gmail.com>
 * @version 1.0
 */

public class Knoten {
    private String name;
    private boolean tiefensucheBesucht;

    public Knoten(String name0) {
        name = name0;
        tiefensucheBesucht = false;
    }

    public String gibName() {
        return name;
    }

    public void markierungSetzen(boolean b) {
        tiefensucheBesucht = b;
    }

    public boolean markierungGeben() {
        return tiefensucheBesucht;
    }
}
