package de.schoolguy.main;

public class Main {

    public static void main(String[] args) {
        Assistent assistent = new Assistent();
        assistent.erzeugeNetz();
        assistent.schreibeMatrix();
        System.out.println("-------------------------------------------");
        assistent.schreibeAlleKnoten();
        System.out.println("-------------------------------------------");
        assistent.schreibeVerbindung("Altd");
        System.out.println("-------------------------------------------");
        assistent.tiefensuche();
    }
}
