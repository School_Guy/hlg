package de.schoolguy.main;

/**
 * Default Description
 *
 * @author SchoolGuy <matrixfueller@gmail.com>
 * @version 1.0
 */

public class Graph {
    private int[][] gewicht;
    private Knoten[] knoten;
    private int knotenZahl;

    public Graph(int maxZahl) {
        gewicht = new int[maxZahl][maxZahl];
        knoten = new Knoten[maxZahl];
        knotenZahl = 0;
        for (int i = 0; i < maxZahl; i++) {
            for (int j = 0; j < maxZahl; j++) {
                if (i == j) {
                    gewicht[i][j] = 0;
                } else {
                    gewicht[i][j] = -1;
                }
            }
        }
    }

    public boolean erzeugeKnoten(String name) {
        if (gibIndex(name) >= 0) {
            return false;
        } else {
            if (knotenZahl >= knoten.length) {
                return false;
            } else {
                knoten[knotenZahl] = new Knoten(name);
                knotenZahl++;
                return true;
            }
        }
    }

    public boolean loescheKnoten(String name) {
        int n = gibIndex(name);
        if (n < 0) {
            return false;
        }
        for (int i = n; i < knotenZahl - 1; i++) {
            knoten[i] = knoten[i + 1];
            for (int j = 0; j < knotenZahl; j++) {
                gewicht[i][j] = gewicht[i + 1][j];
            }
            gewicht[i][knotenZahl - 1] = -1;
        }
        for (int i = n; i < knotenZahl - 1; i++) {
            for (int j = 0; j < knotenZahl - 1; j++) {
                gewicht[j][i] = gewicht[j][i + 1];
            }
            gewicht[knotenZahl - 1][i] = -1;
        }
        knotenZahl--;
        return true;
    }

    private int gibIndex(String name) {
        for (int i = 0; i < knotenZahl; i++) {
            if (name.equals(knoten[i].gibName())) {
                return i;
            }
        }
        return -1;
    }

    public boolean verbinde(String name1, String name2, int bewertung) {
        int n1, n2;
        n1 = gibIndex(name1);
        n2 = gibIndex(name2);
        if ((n1 < 0) || (n2 < 0) || (n1 == n2)) {
            return false;
        } else {
            gewicht[n1][n2] = bewertung;
            gewicht[n2][n1] = bewertung;
            return true;
        }
    }

    public boolean entferneKante(String name1, String name2) {
        int n1, n2;
        n1 = gibIndex(name1);
        n2 = gibIndex(name2);
        if ((n1 < 0) || (n2 < 0) || (n1 == n2)) {
            return false;
        } else {
            gewicht[n1][n2] = -1;
            gewicht[n2][n1] = -1;
            return true;
        }
    }

    public void schreibeKanten(String name) {
        int n = gibIndex(name);
        if (n < 0) {
            System.out.println("Ort nicht in Liste.");
        } else {
            System.out.println("Verbindungen von " + knoten[n].gibName() + " nach ");
            for (int i = 0; i < knotenZahl; i++) {
                if (gewicht[n][i] > 0) {
                    System.out.println("   " + knoten[i].gibName() + ": " + gewicht[n][i] + " km");
                }
            }
        }
    }

    public void schreibeMatrix() {
        System.out.print("       ");
        for (int i = 0; i < knotenZahl; i++) {
            System.out.format("%1$7s", knoten[i].gibName());
            // Formatstring: %1$: 1.String   7s: 7 Zeichen als String
        }
        System.out.println();
        for (int i = 0; i < knotenZahl; i++) {
            System.out.format("%1$7s", knoten[i].gibName());
            for (int j = 0; j < knotenZahl; j++) {
                System.out.format("%1$7d", gewicht[i][j]);
                // Formatstring: %1$: 1.String   7d: 7 Zeichen als Dezimalzahl
            }
            System.out.println();
        }
    }

    public void schreibeAlleKnoten() {
        for (int i = 0; i < knotenZahl; i++) {
            System.out.format("%1$7s", knoten[i].gibName());
            // Formatstring: %1$: 1.String   7s: 7 Zeichen als String
        }
        System.out.println();
    }

    public void tiefensuche(int startNr) {
        tiefensucheKnoten(startNr);
    }

    private void tiefensucheKnoten(int vIndex) {
        knoten[vIndex].markierungSetzen(true);
        System.out.println("Knoten " + knoten[vIndex].gibName() + " besucht!");
        for (int i = 0; i < knotenZahl; i++) {
            if (gewicht[vIndex][i] != -1 && !knoten[i].markierungGeben()) {
                tiefensuche(i);
            }
        }
    }

}

